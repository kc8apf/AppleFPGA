`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/07/2018 01:25:34 PM
// Design Name: 
// Module Name: IS61LV25616AL
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module IS61LV25616AL(
    input CLK,
    input [17:0] A,
    inout [15:0] IO,
    input CE_N,
    input OE_N,
    input WE_N,
    input LB_N,
    input UB_N
    );
    
    localparam RAM_WIDTH = 16;
    localparam RAM_DEPTH = 262144;
    
    reg [RAM_WIDTH-1:0] BRAM [RAM_DEPTH-1:0];
    reg [RAM_WIDTH-1:0] READ_BUFFER;

    generate
        integer ram_index;
        initial
            for (ram_index = 0; ram_index < RAM_DEPTH; ram_index = ram_index + 1)
                BRAM[ram_index] = {RAM_WIDTH{1'b0}};
    endgenerate
    
    // Data bus is only driven during a read. Even then, only the enabled bytes are driven.
    assign IO[7:0] = ({WE_N, CE_N, OE_N, LB_N} == 4'b1000) ? BRAM[A][7:0] : 8'bZ;
    assign IO[15:8] = ({WE_N, CE_N, OE_N, UB_N} == 4'b1000) ? BRAM[A][15:8] : 8'bZ;
    
  always @(posedge CLK) begin
      if ({WE_N, CE_N, LB_N} == 4'b000) BRAM[A][7:0] <= IO[7:0];
      if ({WE_N, CE_N, UB_N} == 4'b000)  BRAM[A][15:8] <= IO[15:8];
  end
endmodule
