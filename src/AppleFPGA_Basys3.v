module AppleFPGA_Basys3(
    input clk, // 100MHz
    
    // VGA
    output [3:0] vga_red,
    output [3:0] vga_green,
    output [3:0] vga_blue,
    output vga_hsync,
    output vga_vsync,
    
    // PS/2
    input ps2_clk,
    input ps2_data,

    // Serial Ports
    output rs232_tx,
    input rs232_rx,
    
    // Display
    output [3:0] seven_seg_digit_n,
    output [6:0] seven_seg_segment_n,
    output seven_seg_dp,
    
    // LEDs
    output [7:0] led,
    
    // Extra Buttons and Switches
    input [7:0] switch,				//  7 System type 1	Not used
                                    //  6 System type 0	Not used
                                    //  5 Serial Port speed
                                    //  4 Swap floppy
                                    //  3 Write protect floppy 2
                                    //  2 Write protect floppy 1
                                    //  1 CPU_SPEED[1]
                                    //  0 CPU_SPEED[0]
    
    input [3:0] button				//  3 RESET
                                    //  2 Not used
                                    //  1 Closed Apple
                                    //  0 Open Apple 
);

reg clk_div2;

always @(posedge clk)
begin
    clk_div2 <= ~clk_div2;
end

// Basys3 has 4-bit VGA while AppleFPGA only provides 1-bit natively.  Map the 1 bit to on/off in 4-bit.
wire vga_1bit_red;
wire vga_1bit_green;
wire vga_1bit_blue;

assign vga_red = {vga_1bit_red, vga_1bit_red, vga_1bit_red, vga_1bit_red};
assign vga_green = {vga_1bit_green, vga_1bit_green, vga_1bit_red, vga_1bit_green};
assign vga_blue = {vga_1bit_blue, vga_1bit_blue, vga_1bit_blue, vga_1bit_blue};


wire [17:0] ram_address;
wire ram_output_enable_n;
wire ram_write_enable_n;

// RAM0
wire ram0_cs_n;
wire [1:0] ram0_byte_enable_n;
wire [15:0] ram0_data;

IS61LV25616AL RAM0(
    .CLK(clk),
    .A(ram_address),
    .IO(ram0_data),
    .CE_N(ram0_cs_n),
    .OE_N(ram_output_enable_n),
    .WE_N(ram_write_enable_n),
    .LB_N(ram0_byte_enable_n[0]),
    .UB_N(ram0_byte_enable_n[1])
);


// RAM1
wire ram1_cs_n;
wire [1:0] ram1_byte_enable_n;
wire [15:0] ram1_data;

IS61LV25616AL RAM1(
    .CLK(clk),
    .A(ram_address),
    .IO(ram1_data),
    .CE_N(ram1_cs_n),
    .OE_N(ram_output_enable_n),
    .WE_N(ram_write_enable_n),
    .LB_N(ram1_byte_enable_n[0]),
    .UB_N(ram1_byte_enable_n[1])
);

AppleFPGA(
    .CLK50MHZ(clk_div2),
    // RAM, ROM, and Peripherials
    .RAM_DATA0(ram0_data),				// 16 bit data bus to RAM 0
    .RAM_DATA1(ram1_data),				// 16 bit data bus to RAM 1
    .RAM_ADDRESS(ram_address),			// Common address
    .RAM_RW_N(ram_write_enable_n),				// Common RW
    .RAM0_CS_N(ram0_cs_n),				// Chip Select for RAM 0
    .RAM1_CS_N(ram1_cs_n),				// Chip Select for RAM 1
    .RAM0_BE0_N(ram0_byte_enable_n[0]),				// Byte Enable for RAM 0
    .RAM0_BE1_N(ram0_byte_enable_n[1]),				// Byte Enable for RAM 0
    .RAM1_BE0_N(ram1_byte_enable_n[0]),				// Byte Enable for RAM 1
    .RAM1_BE1_N(ram1_byte_enable_n[1]),				// Byte Enable for RAM 1
    .RAM_OE_N(ram_output_enable_n),
    // VGA
    .RED(vga_1bit_red),
    .GREEN(vga_1bit_green),
    .BLUE(vga_1bit_blue),
    .H_SYNC(vga_hsync),
    .V_SYNC(vga_vsync),
    
    // PS/2
    .ps2_clk(ps2_clk),
    .ps2_data(ps2_data),
    
    //Serial Ports
    .TXD1(rs232_tx),
    .RXD1(rs232_rx),
    
    // Display
    .DIGIT_N(seven_seg_digit_n),
    .SEGMENT_N({seven_seg_dp, seven_seg_segment_n}),
    
    // LEDs
    .LED(led),
    
    // Apple Perpherial
    .SPEAKER(),
    .PADDLE(),
    .P_SWITCH(),
    .DTOA_CODE(),
    
    // Extra Buttons and Switches
    .SWITCH(switch),
    .BUTTON(button)
);

endmodule