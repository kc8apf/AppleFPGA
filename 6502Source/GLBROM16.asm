;This is 6502 assembler code for a ProDOS compatible
;Apple II bootable serial drive.
;


;Calculate I/O addresses for this slot
#define sdrive_7    $70

;Hardware addresses
#define UART        $C0F0     ; Slot 5 UART base address
;#define SCREEN      $0400

#define ROM_WP $C060
#define OPT_ROM $C006
#define INT_ROM $C007
#define INTC3ROM $C00A
#define SLOT3ROM $C00B

#define ZERO        $00

; Slot 4 IO locations
#define YEAR_TENS			$C0C2
#define YEAR_ONES			$C0C3
#define MONTH_TENS		$C0C4
#define MONTH_ONES		$C0C5
#define DAY_WEEK			$C0C6
#define DAY_TENS			$C0C7
#define DAY_ONES			$C0C8
#define HOUR_TENS			$C0C9
#define HOUR_ONES			$C0CA
#define MIN_TENS			$C0CB
#define MIN_ONES			$C0CC
#define SEC_TENS			$C0CD
#define SEC_ONES			$C0CE


; Slot 7 RAM locations
#define sram0_7     $47F
#define CHECKSUM    $47F	; Storage for calculating checksums
#define sram1_7     $4FF
#define BLOCK_HI    $4FF	; Storage for the calculated high block address
#define sram2_7     $57F
#define sram3_7     $5FF
#define sram4_7     $67F
#define sram5_7     $6FF
#define sram6_7     $77F
#define sram7_7     $7FF

;ProDOS defines
#define command     $42       ;ProDOS command
#define unit        $43       ;7=drive 6-4=slot 3-0=not used
#define buflo       $44       ;low address of buffer
#define bufhi       $45       ;hi address of buffer
#define blklo       $46       ;low block
#define blkhi       $47       ;hi block
#define ioerr       $27       ;I/O error code
#define nodev       $28       ;no device connected
#define wperr       $2B       ;write protect error
#define READ_BLOCK  1         ;ProDOS read block command
#define WRITE_BLOCK 2         ;ProDOS write block command
#define monitor     $FF69

; SERIAL PORT BOOT
; This code is used to load the Apple ROMS from the highest
; 37 blocks of the serial drive. It is only needed if the
; ROMs are not precompiled into the FPGA.

	.ORG	$FF00
; Relocate the code to page 3
START
	cld
	lda	#$15	         ;Set to 8-N-1
	sta	UART
	lda	UART+1
	ldx	#$FF
	txs
boot_loop0
	lda	START,X
	sta	$0300,X
	dex
	bne	boot_loop0
	jmp	START_PG0
FINISHED
	.org FINISHED - START + $300
START_PG0
	lda	#$02			; set default color to green
	sta	ROM_WP
	sta	OPT_ROM
	sta	SLOT3ROM
	lda	#$DC			;block FFDC?
	sta	blklo
	lda	#$FF
	sta	blkhi
	stx	buflo			;x should already have 0
boot_loop1
	lda	ROMX,X
	bne	continue
	inx
	sta	INT_ROM
	sta	INTC3ROM
	lda	ROMX,X
	beq	FINISH
continue
	sta	bufhi
	jsr	get512
	inc	blklo
	inx
	bne	boot_loop1		; always branch
FINISH
	lda	ROM_WP
	sta	OPT_ROM
	jmp	($FFFC)

get512
	lda	#$READ_BLOCK	;send command
	jsr	write_byte		;command
	sta	CHECKSUM
	lda	blklo
	jsr	write_byte		;block low
	eor	CHECKSUM
	sta	CHECKSUM
	lda	blkhi
	jsr	write_byte		;block high
	eor	CHECKSUM
	jsr	write_byte		;checksum
	jsr	boot_read_byte	;command
	jsr	boot_read_byte	;blklo
	jsr	boot_read_byte	;blkhi
	jsr	boot_read_byte	;checksum
	ldy	#$00
boot_loop3
	jsr	boot_read_byte
	sta	(buflo),y
	iny
	bne	boot_loop3
	inc	bufhi
boot_loop4
	jsr	boot_read_byte
	sta	(buflo),y
	iny
	bne	boot_loop4
	jsr	boot_read_byte	;checksum
	rts

write_byte
	pha
boot_wait
	lda	UART
	lsr	a
	lsr	a
	bcc	boot_wait
	pla
	sta	UART+1
	rts

boot_read_byte
	lda	UART
	lsr	a
	bcc	boot_read_byte
	lda	UART+1
	rts

ROMX	.BYTE $C1
	.BYTE $C3
	.BYTE $C5
	.BYTE $C7
	.BYTE $00
	.BYTE $C1
	.BYTE $C3
	.BYTE $C5
	.BYTE $C7
	.BYTE $C8
	.BYTE $CA
	.BYTE $CC
	.BYTE $CE
	.BYTE $D0
	.BYTE $D2
	.BYTE $D4
	.BYTE $D6
	.BYTE $D8
	.BYTE $DA
	.BYTE $DC
	.BYTE $DE
	.BYTE $E0
	.BYTE $E2
	.BYTE $E4
	.BYTE $E6
	.BYTE $E8
	.BYTE $EA
	.BYTE $EC
	.BYTE $EE
	.BYTE $F0
	.BYTE $F2
	.BYTE $F4
	.BYTE $F6
	.BYTE $F8
	.BYTE $FA
	.BYTE $FC
	.BYTE $FE
	.BYTE $00
	.BYTE $00

; Clock ROM in slot 4. This clock is just ProDOS compatable. Even
; though it looks like a ThunderClock, it does not support the
; different modes set through writing different character to the card.
; Look at the ProDOS Technicial Reference Manual and The ThunderClock
; manual for the details of the clock string. This code adds the seconds
; at the end of the string, just like the ThunderClock, even though
; ProDOS does not support nor need them.

	.ORG	$C400
	php
	sei
	plp
	bit	$FF58
	bvs	DOS
READ_ENTRY_4
	clc
	bcc	READ_TIME
WRITE_ENTRY_4
	bne LBL3
DOS
LBL3
	rts
READ_TIME
	pha
	lda	#','+$80
	sta	$0202
	sta	$0205
	sta	$0208
	sta	$020B
	sta	$020E
	lda	MONTH_TENS
	ora	#$80
	sta	$0200
	lda	MONTH_ONES
	ora	#$80
	sta	$0201
	lda	#'0'+$80		; Day of week tens is always 0
	sta	$0203
	lda	DAY_WEEK
	ora	#$80
	sta	$0204
	lda	DAY_TENS
	ora	#$80
	sta	$0206
	lda	DAY_ONES
	ora	#$80
	sta	$0207
	lda	HOUR_TENS
	ora	#$80
	sta	$0209
	lda	HOUR_ONES
	ora	#$80
	sta	$020A
	lda	MIN_TENS
	ora	#$80
	sta	$020C
	lda	MIN_ONES
	ora	#$80
	sta	$020D
	lda	SEC_TENS
	ora	#$80
	sta	$020F
	lda	SEC_ONES
	ora	#$80
	sta	$0210
	lda	#$8D			; carrage return at the end
	sta	$0211
	ldx	#$0E
	pla
	rts

; Moved to available space in slot 4 ROM because of space limits
; in ROM 7. Since there is more slot ROM space available than there
; are perferials, FPGA Apple does not support paged C800 space.

; Check the command to make sure the PC got it correctly. But since
; the PC software does not calculate checksums, ignore them.

check_cmd
	jsr	read_byte		; command
	cmp	command
	bne	cmd_error
	jsr	read_byte		; block lo
	cmp	blklo
	bne	cmd_error
	jsr	read_byte		; block hi
	cmp	BLOCK_HI
	bne	cmd_error
	jsr	read_byte		; checksum
	lda	#ZERO			; clear A
	rts
cmd_error
	lda	#$01
	rts

; Read a byte from the UART. There is no checking here to make sure
; a byte comes in within a certain time. If there is a communications
; error, the machine will usually freeze in this subroutine.

read_byte
	lda	UART
	lsr	a
	bcc	read_byte
	lda	UART+1
	rts

; Write a byte to the UART. Wait until buffer is empty then write the byte

wrt_byte
	pha
wait
	lda	UART
	lsr	a
	lsr	a
	bcc	wait
	pla
	sta	UART+1
	rts

; Using the write byte routine, write a block (512 bytes).

write_blk_7
	ldy	#ZERO
	sty	CHECKSUM
loop2
	lda	(buflo),y
	jsr	wrt_byte
	iny
	bne	loop2
	inc	bufhi
loop3
	lda	(buflo),y
	jsr	wrt_byte
	iny
	bne	loop3
	dec	bufhi
	lda	CHECKSUM
	jsr	wrt_byte			; checksum
	jsr	check_cmd
not_error
	clc
	lda	#ZERO
	rts

; Serial Drive ROM code in slot 7. This gives support for status, reads, and
; writes. Formatting is not supported. This version supports two 16 Meg
; drives in one 32 Meg file on the PC. The PC software does not know any
; thing about the ProDOS drive. It only works with blocks of data.

	.ORG	$C700
;ID bytes for booting and drive detection
	cpx	#$20			;ID bytes for ProDOS and the
	cpx	#$00			; Apple Autostart ROM
	cpx	#$03
	cpx	#$3C			;this one for older II's
;load first block and execute to boot
boot_7
	lda	#READ_BLOCK		;set read command
	sta	command
	lda	#sdrive_7		;set slot and unit
	sta	unit
	lda	#$00			;block 0
	sta	blklo
	sta	blkhi
	sta	buflo			;buffer at $800
	lda	#$08
	sta	bufhi
	jsr	entry_7		;get the block
	bcs	boot_7
	ldx	#sdrive_7		;set up for slot 5
	jmp	$801			;execute the block

;This is the ProDOS entry point for this card

entry_7
	lda	unit			;unit number is DNNN0000 where NNN
					;is the slot number
	and	#$70			;kill the drive number
	cmp	#sdrive_7		;make sure same as ProDOS
	beq	docmd_7		;yep, do command
no_dev
	sec				;nope, set device not connected
	lda	#nodev
	rts				;go back to ProDOS

docmd_7

; Always setup the UART
	lda	#$15			;Set to 8-N-1
	sta	UART
	lda	UART+1		;kill noise
	lda	unit			;check which drive
	and	#$80
	ora	blkhi
	sta	BLOCK_HI
	lda	command		;get ProDOS command
	beq	getstat_7		;command 0 is GetStatus
	cmp	#$03
	bcs	format
	jmp	read_write
format
	sec				;Format not permitted
	lda	#wperr		;write protect error
	rts				;go back to ProDOS
getstat_7
	lda	#ZERO		;good status
	ldx	#$FF			;7FFF blocks
	ldy	#$7F
	clc				;send back status
	rts

; Both reads and writes come to here
read_write
	lda	#ZERO
	sta	CHECKSUM
	lda	command
	jsr	wrt_byte           ;command
	lda	blklo
	jsr	wrt_byte           ;block low
	lda	BLOCK_HI
	jsr	wrt_byte           ;block high
	lda	CHECKSUM
	jsr	wrt_byte           ;checksum
	ldy	#ZERO
	sty	CHECKSUM
	lda	command
	cmp	#READ_BLOCK		;Decide if this is a read or a write
	beq	read_blk_7		;Do reads
	jmp	write_blk_7		;Do writes

read_blk_7
	jsr	check_cmd
loop0
	jsr	read_byte
	sta	(buflo),y
	iny
	bne	loop0
	inc	bufhi
; Second 256 byte section
loop1
	jsr	read_byte
	sta	(buflo),y
	iny
	bne	loop1
	jsr	read_byte		; Get checksum
not_error_7
	lda	#ZERO
	clc
	rts

; ProDOS drive configuration block
	.org	$C7FC
	.byte	0,0			;0000 blocks = check status
	.byte	$97			;bits 7=removable 6=interruptable 5-4=#drives
					;3=format 2=write 1=read 0=status
	.byte	entry_7&$00FF	;low byte of entry

	.end
